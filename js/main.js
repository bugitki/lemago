$('.feedback-slider').owlCarousel({
    loop: true,
    nav: true,
    dots: true,
    navText: ["<img src='img/left2.png'>","<img src='img/right2.png'>"],
    items: 2,
    responsive:{
        0:{
            items:1
        },
        750:{
            items:2
        }
    }
});
$(document).on('click', 'a[href^="#"]', function (event) {
	event.preventDefault();
	$('html, body').animate({
		 scrollTop: $($.attr(this, 'href')).offset().top
	}, 500);
});
